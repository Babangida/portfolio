const gulp = require('gulp');
const imagemin = require('imagemin');
const sass = require('gulp-sass');
const cleanCSS = require('clean-css');
const uglifyJS = require('uglify-js');

sass.compiler = require('node-sass');


gulp.task('clean', () => {
    return gulp.src('./assets/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css/'))
});

gulp.task('sass:watch', function () {
    gulp.watch('./assets/scss/*.scss', ['sass']);
});