const dotenv = require('dotenv').config();
const express = require('express');
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');
const cors = require('cors');

process.env.PORT = process.env.PORT || 2802;

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.post('/send', (request, response) => {
    const formData = request;
    console.log('Form Data', request.body);

    const transporter = nodemailer.createTransport({
        host: "mx1.privateemail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: process.env.MAIL__USERNAME,
            pass: process.env.MAIL__PASSWORD,
        },
  });

  transporter.sendMail({
    from: `${request.body.fullname} ${request.body.email}`,
    to: 'tsowababangida@gmail.com',
    subject: request.body.purpose,
    text: request.body.message
  }).then((res) => {
    console.log(res);
  }).catch((error) => {
    console.log(error);
  });

    response.end('Form data received');
});


app.listen(process.env.PORT, () =>
  console.log(`Example app listening on port ${process.env.PORT}!`),
);