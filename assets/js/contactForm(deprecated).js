window.onload = (() => {
    const contactFormSubmitButton = document.getElementById('contact__form__submit__button');

    contactFormSubmitButton.addEventListener(('click'), (ev) => {
        ev.preventDefault();
        const form = document.getElementById('contact__form');
        const contactFormFullname = document.getElementById('contact__form__input__fullname').value;
        const contactFormEmail = document.getElementById('contact__form__input__email').value;
        const contactFormPurpose = document.getElementById('contact__form__input__purpose').value;
        const contactFormMessage = document.getElementById('contact__form__input__message').value;
    
        const contactFormData = {
            fullname: contactFormFullname,
            email: contactFormEmail,
            purpose: contactFormPurpose,
            message: contactFormMessage
        }
        console.log(contactFormData)
        fetch('http://localhost:2802/send', {headers: {'Content-Type': 'application/json'}, method: 'POST', body: JSON.stringify(contactFormData)}).then((res) => {
            console.log(res);
        });
    });
})();