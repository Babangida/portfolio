window.onload = () => {
    // GET MENU BUTTONS
    const openButton = document.getElementById('mobile-nav__main__button--open');
    const closeButton = document.getElementById('mobile-nav__main__button--close');
    // GET MENU LIST
    const menuItems = document.getElementById('mobile-nav__items');
    const menuLinks = document.getElementsByClassName('mobile__link');

    for (var i = 0; i < menuLinks.length; i++) {
        menuLinks[i].addEventListener('click', (ev) => {
            closeButton.click();
        });
    }

    // menuItem.forEach(item => {
    //     item.addEventListener('click', (ev) => {
    //         closeButton.click();
    //     });
    // });

    openButton.addEventListener('click', (ev) => {
        openButton.classList.toggle('active');
        openButton.classList.toggle('inactive');
        closeButton.classList.remove('inactive');
        closeButton.classList.add('active');
        menuItems.style.display = 'block';
    });

    closeButton.addEventListener('click', (ev) => {
        closeButton.classList.toggle('active');
        closeButton.classList.toggle('inactive');
        openButton.classList.remove('inactive');
        openButton.classList.add('active');
        menuItems.style.display = 'none';
    });
}